
require 'read_file'

module Ldap

  class Ldap

    include Read

    def self.new

      read_file
      @@ldap = Net::LDAP.new :host => $host,
        :port => $port,
      :auth => {
        :method => :simple,
        :username => $userName,
        :password => $password
      }


    end

    def self.cadastrar_usuario  dn, attributes
      @@ldap.open do | ldap |
        @@ldap.add(:dn => dn , :attributes => attributes)
      end
    end




  end

end

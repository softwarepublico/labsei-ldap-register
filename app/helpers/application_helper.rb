module ApplicationHelper

  $redmine_path="#"
  $gitlab_path="#"

  def gitlab_link
	reference_link 
	return $gitlab_path	
  end

  def redmine_link
	reference_link
	return $redmine_path
  end
	
  def  reference_link
	if $redmine_path == "#" || $gitlab_path == "#"

	  arquivo = File.open "config/references.yml" , "r"

	  while  !arquivo.eof?
        	word = arquivo.readline.strip
        	word=word.split /:/,2

        	if word[0] == "redmine"
                  $redmine_path= "http://#{word[1].strip}"
        	end
        	if word[0] =="gitlab"
        	  $gitlab_path = "http://#{word[1].strip}"
       		end
           end
        end
  end
end

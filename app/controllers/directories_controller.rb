require 'rubygems'
require 'net/ldap'
require 'net/smtp'

class DirectoriesController < ApplicationController

  $host=
    $port=
    $userName=
    $password=
    $dc=

  def read_file
    arquivo = File.open "config/ldap.yml", "r"

    while  !arquivo.eof?

      word = arquivo.readline.strip
      word=word.split /:/ , 2

      if word[0] == "host"
        $host= word[1].strip
      end

      if word[0] =="port"
        $port= word[1].strip
      end

      if word[0] =="userName"
        $userName= word[1].strip
      end

      if word[0] =="password"
        $password= word[1].strip
      end

      if word[0] =="dc"
        $dc = word[1].strip
      end

    end

  end


  def init_ldap
    read_file
    ldap = Net::LDAP.new :host => $host,
      :port => $port,
    :auth => {
      :method => :simple,
      :username => $userName,
      :password => $password
    }
  end

  def cadastrar_usuario ldap, dn, attributes
    ldap.open do | ldap |
      ldap.add(:dn => dn , :attributes => attributes)
    end
  end


  def update
    ldap = init_ldap

    password = params[:password]
    password_confirmation = params[:password_confirmation]

    ops = [[:replace, :userPassword , password ]]

    if password != password_confirmation
      flash[:error] = 'Nova senha e Confirmar nova senha devem ser idênticos'
      redirect_to(edit_directories_path)
    elsif ldap.bind  
      ldap.modify  :dn => $dn , :operations => ops
      redirect_to(root_path, notice: 'Usuario atualizado com sucesso' )
    else
      flash[:error] = 'Não foi possível atualizar a senha'
      redirect_to(edit_directories_path)
    end

  end


  def search_user
    user_email = params[:email]

    filter = Net::LDAP::Filter.eq("mail",user_email)
    attrs=["mail","cn","sn","dn","uid","givenName","objectclass"]

    ldap = init_ldap
    if ldap.bind
      ldap.search(:base => $dc, :filter => filter,:attributes => attrs , :return_result => true) do |user|
        $dn= "#{user.dn}"
        $uid = "#{user.uid[0]}"
        $givenName = "#{user.givenName[0]}"
        $sn = "#{user.sn[0]}"
        $cn = "#{user.cn[0]}"
        $mail="#{user.mail[0]}"
      end
    end

    if  $dn
      redirect_to(edit_directories_path, notice: 'Usuario encontrado com sucesso' )
    else
      flash[:error] = 'Usuário não encontrado'
      redirect_to(root_path)
    end

  end

  def is_registered ldap, login, password
    @result = ldap.bind_as(
      :base => $dc,
      :filter => "(uid=#{login})",
      :password => password
    )
  end

  def set_attributes login, nome, sobreNome, email, password

    attributes = {
      :uid => login,
      :objectClass => [ "organizationalPerson",
                        "person",
                        "top",
                        "inetOrgPerson",
                        "shadowAccount"],
      :sn => sobreNome,
      :givenName => nome,
      :mail => email ,
      :cn => "#{nome} #{sobreNome}" ,
      :userPassword => "#{password}"
    }

  end


  def create

    login = params[:login]
    nome = params[:nome]
    sobreNome = params[:sobreNome]
    email = params[:email]
    password = params[:password]
    password_confirmation = params[:password_confirmation]

    if password_confirmation == password
      dn = "uid=#{login},ou=users,#{$dc}"
      ldap = init_ldap
      attributes = set_attributes login,nome,sobreNome,email,password
      cadastrar_usuario ldap, dn, attributes
      is_registered ldap, login, password
    else
      flash[:error] = 'Campo senha deve ser idêntico ao confirmar senha'
    end

  end


end
